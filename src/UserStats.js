/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment, PureComponent } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  FlatList,
  TouchableOpacity
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';




class UserStats extends PureComponent {
  constructor(props) {
    super(props)
  }
  state = {

  };
  back() {
    this.props.navigation.navigate('MainScreen')
  }
  render() {
    console.log("hello")
    return (
      <View style={{ width: '100%', height: '100%' }}>
        <View style={{ flex: .1, flexDirection: 'row' }}>
          <TouchableOpacity style={{ flex: .2, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.back()}>
            <Text>Back</Text>
          </TouchableOpacity>
          <View style={{ flex: .6, justifyContent: 'center', alignItems: 'center' }}>
            <Text>User Stats</Text>
          </View>
          <View style={{ flex: .2 }}>

          </View>
        </View>

        <View style={{flex: .9}}>
          <View style={{flexDirection: 'row'}}>
              <Text>Answers Correct: </Text>
              <Text>20</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
              <Text>Total Quiz Time: </Text>
              <Text>20</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
              <Text>Flagged Quiz Time: </Text>
              <Text>20</Text>
          </View>
          <View>
            
          </View>
        </View>
      </View>
    );
  }


}



export default UserStats;