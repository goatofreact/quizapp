/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment, PureComponent } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  FlatList,
  TouchableOpacity,
  TextInput
} from 'react-native';



import { NetInfo, UserJson, WebJson } from './constants/strings'


class QuizForm extends PureComponent {
  constructor(props) {
    super(props)
  }
  state = {
    userName: '',
    password: '',
    secondAnswer: '',
    thirdAnswer: '',
    fourthAnswer: '',
  };

  render() {
    console.log("hello")
    return (
      <View style={{ width: '100%', height: '100%' }}>

        <View style={{ flex: 1, flexDirection: 'column', margin: 20, justifyContent: 'center' }}>
          <Text>User Name</Text>
          <View>
            <TextInput
              style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
              onChangeText={text => this.setState({ userName: text })}
              value={this.state.userName}
            />
          </View>
          <Text>Password</Text>
          <View>

            <TextInput
              style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
              onChangeText={text => this.setState({ password: text })}
              value={this.state.password}
            />
          </View>
          <TouchableOpacity onPress={() => this.submit()}>
            <Text>Submit</Text>
          </TouchableOpacity>


        </View>
      </View>
    );
  }

  submit = async () => {
    var data = {}
    data[UserJson.KEY_USER_NAME] = 'Vito'
    data[UserJson.KEY_USER_FACEBOOK_TOKEN] = 'tokenVito'
    console.log(NetInfo.SERVER_IP + ':' + NetInfo.SERVER_PORT + '/' + WebJson.WEB_LOGIN)
    console.log(data)
    try {
      const response = await fetch(NetInfo.SERVER_IP + ':' + NetInfo.SERVER_PORT + '/' + WebJson.WEB_LOGIN, {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers: {
          'Content-Type': 'application/json'
        }
      });
      const json = await response.json();
      this.props.navigation.navigate('MainScreen', json)
      console.log('Success:', json);
    } catch (error) {
      console.error('Error:', error);
    }
    
  }

  loadQuizes = async () => {
    var data = {}
    data[UserJson.KEY_USER_NAME] = 'Vito'
    data[UserJson.KEY_USER_ID] = 1
    data[UserJson.KEY_USER_FACEBOOK_TOKEN] = 'tokenVito'
    data[WebJson.WEB_QUIZ_COMPLETED] = true
    console.log(NetInfo.SERVER_IP + ':' + NetInfo.SERVER_PORT + '/' + WebJson.WEB_QUIZZES)
    console.log(data)
    try {
      const response = await fetch(NetInfo.SERVER_IP + ':' + NetInfo.SERVER_PORT + '/' + WebJson.WEB_QUIZZES, {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers: {
          'Content-Type': 'application/json'
        }
      });
      const json = await response.json();
      console.log('Success:', JSON.stringify(json));
    } catch (error) {
      console.error('Error:', error);
    }
    this.props.navigation.navigate('MainScreen')
  }

  loadQuizesBy = async () => {
    var data = {}
    data[UserJson.KEY_USER_NAME] = 'Vito'
    data[WebJson.WEB_QUIZ_CREATOR] = 1
    data[UserJson.KEY_USER_ID] = 1
    data[UserJson.KEY_USER_FACEBOOK_TOKEN] = 'tokenVito'
    data[WebJson.WEB_QUIZ_COMPLETED] = false
    console.log(NetInfo.SERVER_IP + ':' + NetInfo.SERVER_PORT + '/' + WebJson.WEB_QUIZZES)
    console.log(data)
    try {
      const response = await fetch(NetInfo.SERVER_IP + ':' + NetInfo.SERVER_PORT + '/' + WebJson.WEB_QUIZZES, {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers: {
          'Content-Type': 'application/json'
        }
      });
      const json = await response.json();
      console.log('Success:', JSON.stringify(json));
    } catch (error) {
      console.error('Error:', error);
    }
    this.props.navigation.navigate('MainScreen')
  }

  loadQuiz = async () => {
    var data = {}
    data[UserJson.KEY_USER_NAME] = 'Vito'
    data[UserJson.KEY_USER_ID] = 1
    data[UserJson.KEY_USER_FACEBOOK_TOKEN] = 'tokenVito'
    data['quizId'] = 1
    console.log(NetInfo.SERVER_IP + ':' + NetInfo.SERVER_PORT + '/' + WebJson.QUIZ)
    console.log(data)
    try {
      const response = await fetch(NetInfo.SERVER_IP + ':' + NetInfo.SERVER_PORT + '/' + WebJson.WEB_QUIZZES, {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers: {
          'Content-Type': 'application/json'
        }
      });
      const json = await response.json();
      console.log('Success:', JSON.stringify(json));
    } catch (error) {
      console.error('Error:', error);
    }
    this.props.navigation.navigate('MainScreen')
  }
}



export default QuizForm;