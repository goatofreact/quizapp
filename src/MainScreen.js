/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment, PureComponent } from 'react';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';


import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  FlatList,
  TouchableOpacity
} from 'react-native';

import Swiper from 'react-native-swiper'
import { Rating, AirbnbRating } from 'react-native-ratings';
import { NetInfo, UserJson, WebJson } from './constants/strings'

class MyListItem extends PureComponent {
  _onPress = () => {
    this.props.onPressItem(this.props.id);
  };

  // {"difficulty":-1,"flagged":0,"quizId":5,"quizCreatorId":2,"funFactor":-1,"quizTitle":"Title 5","relevance":-1}]}
  render() {
    const { difficulty, funFactor, relevance, quizTitle, quizId } = this.props.item
    console.log("Yello")
    return (
      <View style={{ flexDirection: 'row', margin: 5, backgroundColor: 'purple' }} onPress={this._onPress}>
        <TouchableOpacity onPress={() => this.props.gotoQuizScren(quizId)} style={{ flex: .8, flexDirection: 'column', padding: 10, }}>
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
            <Text>{quizTitle}</Text>
          </View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: .3333, flexDirection: 'column', justifyContent: 'center' }}>
              <View >
                <Text  >Difficulty</Text>
              </View>

              <Rating
                imageSize={20}
                readonly
                ratingCount={3}
                startingValue={difficulty}

                tintColor='purple'
              />
            </View>
            <View style={{ flex: .3333, flexDirection: 'column', justifyContent: 'center' }}>
              <Text>Fun Factor</Text>
              <Rating
                type='custom'
                imageSize={20}
                readonly
                ratingCount={3}
                startingValue={funFactor}
                tintColor='purple'
              />
            </View>
            <View style={{ flex: .3333, flexDirection: 'column', justifyContent: 'center' }}>
              <Text>Relevance</Text>
              <Rating
                type='custom'
                imageSize={20}
                readonly
                ratingCount={3}
                startingValue={relevance}
                tintColor='purple'
              />
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={{ flex: .2, justifyContent: 'center', alignItems: 'center', backgroundColor: 'green' }}>
          <Text>Share</Text>
        </TouchableOpacity>
      </View>
    );
  }
}






class MainScreen extends PureComponent {
  constructor(props) {
    super(props)
    console.log(props.navigation)
  }
  state = {
    newData: false,
    data: [{ title: 'Know Your Cars', id: 1, rating: 4 }, { title: 'Know Your House', id: 2, rating: 3 }, { title: 'Know Your Bikes', id: 3, rating: 2 }],
    quizesComplete: [],
    quizzesNotComplete: []
  };

  componentDidMount() {
    this.loadQuizes(true)
    this.loadQuizes(false)
  }
  render() {
    console.log("hello")
    return (
      <View style={{ width: '100%', height: '100%' }}>
        <View style={{ flex: .1, flexDirection: 'row', backgroundColor: 'red' }}>
          <View style={{ flex: .8 }}>

          </View>
          <View style={{ flex: .2, justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('LoginScreen')}>
              <Text>Logout</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ flex: .7, margin: 3, flexDirection: 'column', backgroundColor: 'green' }}>
          <Swiper style={{ flex: 1 }} showsButtons={false}>
            <View style={{ flex: 1 }}>
              <View style={{ flex: .1 }}>
                <Text>Completed</Text>
              </View>
              <FlatList
                style={{ flex: .9, backgroundColor: 'gray' }}
                data={this.state.quizesComplete}
                extraData={this.state}
                keyExtractor={this._keyExtractor}
                renderItem={this._renderItem}
              />
            </View>
            <View style={{ flex: 1 }}>
              <View style={{ flex: .1 }}>
                <Text>Not Complete</Text>
              </View>
              <FlatList
                style={{ flex: .9, backgroundColor: 'gray' }}
                data={this.state.quizzesNotComplete}
                extraData={this.state}
                keyExtractor={this._keyExtractor}
                renderItem={this._renderItem}
              />
            </View>
          </Swiper>


        </View>
        <View style={{ flex: .2, margin: 10, flexDirection: 'row', flexdirection: 'row', backgroundColor: 'yellow' }}>
          <TouchableOpacity onPress={() => this.gotoUserState()} style={{ flex: .33333, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', backgroundColor: 'darkgreen' }}>
            <Text>User Stats</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.gotoQuizForm()} style={{ flex: .33333, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', backgroundColor: 'cyan' }}>
            <Text>Creat Quiz</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.gotoUserState()} style={{ flex: .33333, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', backgroundColor: 'deeppink' }}>
            <Text>Other</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  loadQuizes = async (completed) => {
    var data = {}
    data[UserJson.KEY_USER_NAME] = 'Vito'
    data[UserJson.KEY_USER_ID] = 1
    data[UserJson.KEY_USER_FACEBOOK_TOKEN] = 'tokenVito'
    data[WebJson.WEB_QUIZ_COMPLETED] = completed
    console.log(NetInfo.SERVER_IP + ':' + NetInfo.SERVER_PORT + '/' + WebJson.WEB_QUIZZES)
    console.log(data)
    try {
      const response = await fetch(NetInfo.SERVER_IP + ':' + NetInfo.SERVER_PORT + '/' + WebJson.WEB_QUIZZES, {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers: {
          'Content-Type': 'application/json'
        }
      });
      const json = await response.json();
      console.log('Success:', JSON.stringify(json));

      if (completed) {
        this.setState({ quizesComplete: json[WebJson.WEB_GAME_LIST] })
      } else {
        this.setState({ quizzesNotComplete: json[WebJson.WEB_GAME_LIST] })
      }
    } catch (error) {
      console.error('Error:', error);
    }
  }

  _keyExtractor = (item, index) => String(index);

  _onPressItem = (id) => {

  };

  _renderItem = ({ item }) => (
    <MyListItem 
      item={item}
      onPressItem={this._onPressItem}
      title={item.title}
      gotoQuizScren={(quizId) => this.gotoQuizScreen(quizId)}
    />
  );

  gotoUserState() {
    this.props.navigation.navigate('UserStats')
  }
  gotoQuizForm() {
    this.props.navigation.navigate('QuizForm')
  }
  gotoQuizScreen(quizId) {
    this.props.navigation.navigate('QuizScreen', {quizId})
  }

}



export default MainScreen;