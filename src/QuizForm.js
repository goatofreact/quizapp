
import React, { Fragment, PureComponent } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    FlatList,
    TouchableOpacity,
    TextInput, 
    KeyboardAvoidingView
} from 'react-native';

import {
    Header,
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Swiper from 'react-native-swiper'

import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button'
import { Rating, AirbnbRating } from 'react-native-ratings';
import { QuizJson } from './constants/strings'

class QuizFormPage extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            question: '',
            firstAnswer: '',
            secondAnswer: '',
            thirdAnswer: '',
            fourthAnswer: '',
            complete: false,
        };

    }



    render() {
        console.log("render")
        let isComplete = null
        if (this.state.complete) {
            isComplete = (<View><Text style={{ color: 'green' }}>Complete!</Text></View>)
        }
        return (
            <View style={{ flex: .8, flexDirection: 'column', margin: 30}}>
                {isComplete}
                <Text>Enter Question</Text>
                <View style={{marginBottom: 20}}>
                    <TextInput
                        style={{ height: 40, borderColor: 'gray', borderWidth: 1, borderRadius: 10 }}
                        onChangeText={text => this.setState({ question: text })}
                        value={this.state.question}
                    />
                </View>
                <Text>Enter First Answer</Text>
                <View>

                    <TextInput
                        style={{ height: 40, borderColor: 'gray', borderWidth: 1, borderRadius: 10 }}
                        onChangeText={text => this.setState({ firstAnswer: text })}
                        value={this.state.firstAnswer}
                    />
                </View>
                <Text>Enter First Answer</Text>
                <View>
                    <TextInput
                        style={{ height: 40, borderColor: 'gray', borderWidth: 1, borderRadius: 10 }}
                        onChangeText={text => this.setState({ secondAnswer: text })}
                        value={this.state.secondAnswer}
                    />
                </View>
                <Text>Enter First Answer</Text>
                <View>
                    <TextInput
                        style={{ height: 40, borderColor: 'gray', borderWidth: 1, borderRadius: 10 }}
                        onChangeText={text => this.setState({ thirdAnswer: text })}
                        value={this.state.thirdAnswer}
                    />
                </View>
                <Text>Enter First Answer</Text>
                <View>
                    <TextInput
                        style={{ height: 40, borderColor: 'gray', borderWidth: 1, borderRadius: 10 }}
                        onChangeText={text => this.setState({ fourthAnswer: text })}
                        value={this.state.fourthAnswer}
                    />
                </View>
                <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center', margin: 20}}onPress={() => this.onSave()}>
                    <Text style={{ borderWidth: 2, borderRadius: 20, padding: 10}}>Save Question</Text>
                </TouchableOpacity>
            </View>
        )
    }

    onSave() {
        console.log("onSave", this.state.question, this.state.question.length > 0, this.state.firstAnswer.length > 0, this.state.secondAnswer.length > 0, this.state.thirdAnswer.length > 0, this.state.fourthAnswer.length > 0)
        if (this.state.question.length > 0 && this.state.firstAnswer.length > 0 && this.state.secondAnswer.length > 0 && this.state.thirdAnswer.length > 0 && this.state.fourthAnswer.length > 0) {
            console.log("onSave")
            this.setState({ complete: true })
            let json = {};
            json[QuizJson.KEY_QUESTION_QUESTION] = this.state.question
            json[QuizJson.KEY_QUESTION_ANSWER_CORRECT] = this.state.firstAnswer
            json[QuizJson.KEY_QUESTION_ANSWER_WRONG1] = this.state.secondAnswer
            json[QuizJson.KEY_QUESTION_ANSWER_WRONG2] = this.state.thirdAnswer
            json[QuizJson.KEY_QUESTION_ANSWER_WRONG3] = this.state.fourthAnswer
            this.props.saveQuestion(json)
        }
    }

}



class QuizForm extends PureComponent {
    constructor(props) {
        super(props)
        this.questions = new Map()
        this.state = {
            title: ''
        }
    }

    render() {
        return (
            <KeyboardAvoidingView style={{ width: '100%', height: '100%' }}>
                <View style={{ flex: .1, flexDirection: 'row' }}>
                    <TouchableOpacity style={{ flex: .2, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.back()}>
                        <Text>Back</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flex: .2, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.save()}>
                        <Text style={{ borderWidth: 2, borderRadius: 20, padding: 5}}>Save Quiz</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flex: .9 }}>
                    <View style={{ flexDirection: 'row' , backgroundColor: 'lightblue'}}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', padding: 10 }}>
                            <Text> Quiz Title </Text>
                        </View>

                        <TextInput
                            style={{ flex: 1, height: 40, borderColor: 'gray', borderWidth: 1, borderRadius: 10 }}
                            onChangeText={text => this.setState({ title: text })}
                            value={this.state.title}
                        />
                    </View>
                    <Swiper style={styles.wrapper} showsButtons={true}>
                        <View style={styles.slide3}>
                            <QuizFormPage title='Question' saveQuestion={(question) => this.saveQuestion(question, 0)} />
                        </View>
                        <View style={styles.slide1}>
                            <QuizFormPage title='Question' saveQuestion={(question) => this.saveQuestion(question, 1)} />
                        </View>
                        <View style={styles.slide1}>
                            <QuizFormPage title='Question' saveQuestion={(question) => this.saveQuestion(question, 2)} />
                        </View>
                        <View style={styles.slide1}>
                            <QuizFormPage title='Question' saveQuestion={(question) => this.saveQuestion(question, 3)} />
                        </View>
                        <View style={styles.slide2}>
                            <QuizFormPage title='Question' saveQuestion={(question) => this.saveQuestion(question, 4)} />
                        </View>
                        <View style={styles.slide3}>
                            <QuizFormPage title='Question' saveQuestion={(question) => this.saveQuestion(question, 5)} />
                        </View>
                        <View style={styles.slide1}>
                            <QuizFormPage title='Question' saveQuestion={(question) => this.saveQuestion(question, 6)} />
                        </View>
                        <View style={styles.slide2}>
                            <QuizFormPage title='Question' saveQuestion={(question) => this.saveQuestion(question, 7)} />
                        </View>
                        <View style={styles.slide1}>
                            <QuizFormPage title='Question' saveQuestion={(question) => this.saveQuestion(question, 8)} />
                        </View>
                        <View style={styles.slide3}>
                            <QuizFormPage title='Question' saveQuestion={(question) => this.saveQuestion(question, 9)} />
                        </View>
                    </Swiper>

                </View>
            </KeyboardAvoidingView>
        );
    }
    saveQuestion(question, index) {
        
        this.questions.set(String(index),question)

        console.log(this.questions)
    }
    back() {
        this.props.navigation.navigate('MainScreen')
    }
    save() {
        this.props.navigation.navigate('MainScreen')
    }
}
const styles = StyleSheet.create({
    wrapper: {},
    slide1: {
        flex: 1,
        backgroundColor: '#9DD6EB'
    },
    slide2: {
        flex: 1,
        backgroundColor: '#97CAE5'
    },
    slide3: {
        flex: 1,
        backgroundColor: '#92BBD9'
    },
    text: {
        color: '#000',
        fontSize: 30,
        fontWeight: 'bold'
    }
})


export default QuizForm;