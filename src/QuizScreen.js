
import React, { Fragment, PureComponent } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  FlatList,
  TouchableOpacity
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Swiper from 'react-native-swiper'

import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button'
import { Rating, AirbnbRating } from 'react-native-ratings';
import { QuizJson, UserJson, NetInfo, WebJson } from './constants/strings'


class QuizQuestion extends PureComponent {

  state = {
    value: null
  }
  render() {
    const { quiz } = this.props
    let radioProps = [];
    if (quiz != undefined) {
      radioProps.push({ label: quiz[QuizJson.KEY_QUESTION_ANSWER_CORRECT], value: 0 })
      radioProps.push({ label: quiz[QuizJson.KEY_QUESTION_ANSWER_WRONG1], value: 1 })
      radioProps.push({ label: quiz[QuizJson.KEY_QUESTION_ANSWER_WRONG2], value: 2 })
      radioProps.push({ label: quiz[QuizJson.KEY_QUESTION_ANSWER_WRONG3], value: 3 })
    }
    radioProps = this.shuffle(radioProps)
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
        <View style={{ justifyContent: 'center', alignItems: 'center', margin: 10, backgroundColor: 'white', borderColor: 'black', borderRadius: 10, borderWidth: 3 }}>
          <Text >{quiz[QuizJson.KEY_QUESTION_QUESTION]}</Text>
        </View>



        <RadioForm
          radio_props={radioProps}
          initial={-1}
          onPress={(value) => { this.answerSelected(value) }}
        />
      </View>
    )
  }

  shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }

  answerSelected(value) {
    const { questionAnswered, quizIndex } = this.props
    console.log(value, quizIndex)
    questionAnswered(value, quizIndex)
  }

}

class RatingSection extends PureComponent {
  constructor(props) {
    super(props)
    this.reviews = ["Bad", "Meh", "Good"]
  }
  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
        <Text>Difficulty</Text>
        <AirbnbRating
          count={3}
          reviews={this.reviews}
          onFinishRating={this.ratingCompleted}
          style={{ backgroundColor: '#92BBD9', paddingVertical: 10 }}
        />
        <Text>Funness</Text>
        <AirbnbRating
          count={3}
          reviews={this.reviews}
          onFinishRating={this.ratingCompleted}
          style={{ backgroundColor: '#92BBD9', paddingVertical: 10 }}
        />
        <Text>Relavence</Text>
        <AirbnbRating
          count={3}
          reviews={this.reviews}
          onFinishRating={this.ratingCompleted}
          style={{ backgroundColor: '#92BBD9', paddingVertical: 10 }}
        />
        <Text>Overall</Text>
        <AirbnbRating
          count={3}
          reviews={this.reviews}
          onFinishRating={this.ratingCompleted}
          style={{ backgroundColor: '#92BBD9', paddingVertical: 10 }}
        />
      </View>
    )
  }

  ratingCompleted(rating) {
    console.log("Rating is: " + rating)
  }
}

class QuizScreen extends PureComponent {
  constructor(props) {
    super(props)
    this.answerArray = new Map()
  }
  state = {
    quizArray: [],
    quizItems: [],
    quiz: null,
    time: 0
  };
  back() {
    this.props.navigation.navigate('MainScreen')
  }
  componentDidMount() {
    console.log("didmount")
    this.loadQuiz()
  }
  render() {
    // var quizItems = []
    // // var quizItems = this.state.quizArray.map(function (value, index) {
    // //   return (<View key={index} style={styles.slide1} ><QuizQuestion key={String(index)} quizIndex={index} quiz={value} questionAnswered={() => this.questionAnswered()} /></View>)
    // // })
    // for(var i = 0; i < this.state.quizArray.length; i++){
    //   quizItems.push(<View key={i} style={styles.slide1} ><QuizQuestion key={String(i)} quizIndex={i} quiz={this.state.quizArray[i]} questionAnswered={(answer, i) => this.questionAnswered(answer, i)} /></View>)
    // }
    // quizItems.push(<View key={String(quizItems.length)} style={styles.slide1}><RatingSection /></View>)

    return (
      <View style={{ width: '100%', height: '100%' }}>
        <View style={{ flex: .1, flexDirection: 'row' }}>
          <TouchableOpacity style={{ flex: .2, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.back()}>
            <Text>Back</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ flex: .2, justifyContent: 'center', alignItems: 'center' }} onPress={() => this.quizDone()}>
            <Text>Done</Text>
          </TouchableOpacity>
          <View style={{ flex: .2, justifyContent: 'center', alignItems: 'center', fontSize: 30, color: 'red' }}>
            <Text style={{ fontSize: 30, color: 'red' }}>{this.state.time}</Text>
          </View>
        </View>
        <View style={{ flex: .9 }}>
          <Swiper style={styles.wrapper} key={this.state.quizArray.length + 1} showsButtons={true} loop={false}>
            {this.state.quizItems}
          </Swiper>

        </View>
      </View>
    );
  }


  loadQuiz = async () => {
    const { quizId } = this.props.navigation.state.params
    var data = {}
    data[UserJson.KEY_USER_NAME] = 'Vito'
    data[UserJson.KEY_USER_ID] = 1
    data[UserJson.KEY_USER_FACEBOOK_TOKEN] = 'tokenVito'
    data[QuizJson.KEY_QUIZ_ID] = quizId
    console.log(NetInfo.SERVER_IP + ':' + NetInfo.SERVER_PORT + '/' + WebJson.QUIZ)
    console.log(data)
    try {
      const response = await fetch(NetInfo.SERVER_IP + ':' + NetInfo.SERVER_PORT + '/' + WebJson.WEB_QUIZ, {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers: {
          'Content-Type': 'application/json'
        }
      });
      const json = await response.json();
      console.log('Success:', JSON.stringify(json));
      // this.setState({ quizArray: json[QuizJson.KEY_QUIZ_QUESTIONS] })


      var quizItems = []
      // var quizItems = this.state.quizArray.map(function (value, index) {
      //   return (<View key={index} style={styles.slide1} ><QuizQuestion key={String(index)} quizIndex={index} quiz={value} questionAnswered={() => this.questionAnswered()} /></View>)
      // })
      for (var i = 0; i < json[QuizJson.KEY_QUIZ_QUESTIONS].length; i++) {
        quizItems.push(<View key={i} style={styles.slide1} ><QuizQuestion key={String(i)} quizIndex={i} quiz={json[QuizJson.KEY_QUIZ_QUESTIONS][i]} questionAnswered={(answer, i) => this.questionAnswered(answer, i)} /></View>)
      }
      quizItems.push(<View key={String(quizItems.length)} style={styles.slide1}><RatingSection /></View>)
      this.setState({ quizItems, quiz: json })
      setInterval(() => this.setState({ time: this.state.time + 1 }), 1000)

    } catch (error) {
      console.error('Error:', error);
    }
  }

  questionAnswered(answer, i) {
    this.answerArray.set(i, { answer: answer, time: this.state.time })



  }

  quizDone = async () => {
    if (this.answerArray.size == this.state.quizItems.length - 1) {
      var mapAsc = new Map([...this.answerArray.entries()].sort());
      let quizDuratinos = []
      let quizAnswers = []
      for (var i = 0; i < mapAsc.size; i++) {
        quizDuratinos.push(mapAsc.get(i).time)
        quizAnswers.push(mapAsc.get(i).answer)
      }
      let json = {}
      json[UserJson.KEY_USER_NAME] = 'Vito'
      json[UserJson.KEY_USER_ID] = 1
      json[UserJson.KEY_USER_FACEBOOK_TOKEN] = 'tokenVito'
      json[QuizJson.KEY_QUIZ_ID] = this.state.quiz[QuizJson.KEY_QUIZ_ID]
      json[QuizJson.KEY_QUIZ_DURATIONS] = quizDuratinos
      json[QuizJson.KEY_QUIZ_ANSWERS] = quizAnswers
      json[QuizJson.KEY_QUIZ_ID] = this.state.quiz[QuizJson.KEY_QUIZ_ID]
      console.log(json)

      console.log(NetInfo.SERVER_IP + ':' + NetInfo.SERVER_PORT + '/' + WebJson.WEB_RESULTS)

      try {
        const response = await fetch(NetInfo.SERVER_IP + ':' + NetInfo.SERVER_PORT + '/' + WebJson.WEB_RESULTS, {
          method: 'POST', // or 'PUT'
          body: JSON.stringify(json), // data can be `string` or {object}!
          headers: {
            'Content-Type': 'application/json'
          }
        });
        const json = await response.json();
        console.log('Success:', JSON.stringify(json));
      } catch (error) {
        console.error('Error:', error);
      }

      this.props.navigation.navigate('MainScreen')
    } else {
      alert("Quiz Incomplete!!!!")
    }
  }
}
const styles = StyleSheet.create({
  wrapper: {},
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB'
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5'
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9'
  },
  text: {
    color: '#000',
    fontSize: 30,
    fontWeight: 'bold'
  }
})


export default QuizScreen;