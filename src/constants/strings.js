export const UserJson = {
    KEY_USER_ID: 'userId',
    KEY_USER_NAME: "userName",
    KEY_USER_FACEBOOK_TOKEN: 'fbToken',
    KEY_USER_STATS: 'userStats'
}

export const QuizJson = {

    KEY_DIFFICULTY: "difficulty",
    KEY_FUN_FACTOR: "funFactor",
    KEY_RELEVANCE: "relevance",
    KEY_FLAGGED: "flagged",

    KEY_QUIZ_ID : 'quizId',
    KEY_QUIZ_TITLE: 'quizTitle',
    KEY_QUIZ_CREATOR: 'quizCreator',
    KEY_QUIZ_CREATOR_ID: 'quizCreatorId',
    KEY_QUIZ_GROUP_ID: 'quizQroupId',
    KEY_QUIZ_DIFFICULTY: 'quizDifficulty',
    KEY_QUIZ_FUN_FACTOR: 'quizFunFactor',
    KEY_QUIZ_QUESTIONS: 'quizQuestions',
    KEY_QUIZ_ALLOW_RANDOM: 'quizAllowRandom',
    KEY_QUIZ_DURATIONS: 'quizDurations',
    KEY_QUIZ_ANSWERS: 'quizAnswers',

    KEY_QUESTION_QUESTION: 'questionQuestion',
    KEY_QUESTION_ANSWER_CORRECT: 'questionCorrect',
    KEY_QUESTION_ANSWER_WRONG1: 'questionWrong1',
    KEY_QUESTION_ANSWER_WRONG2: 'questionWrong2',
    KEY_QUESTION_ANSWER_WRONG3: 'questionWrong3',
}

export const WebJson = {
    WEB_FAILURE: "FAILURE",
    WEB_GAME_LIST: "webGameList",
    WEB_QUIZ_COMPLETED: "webQuizCompleted",
    WEB_QUIZ_CREATOR: "webQuizCreator",
    WEB_LOGIN: "login",
    WEB_QUIZZES: "quizzes",
    WEB_QUIZ: "quiz",
    WEB_RESULTS: 'results',
    WEB_REVIEW: 'review'
}

export const NetInfo = {
    SERVER_IP: "http://home.vitoesposito.com",
    SERVER_PORT: "62523"
}



