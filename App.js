import React from 'react';
import { View, Text } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import MainScreen from './src/MainScreen'
import QuizForm from './src/QuizForm'
import QuizScreen from './src/QuizScreen'
import UserStats from './src/UserStats'
import LoginScreen from './src/LoginScreen'

const AppNavigator = createStackNavigator(
  {
  MainScreen: {
    screen: MainScreen,
  },
  QuizForm: {
    screen: QuizForm,
  },
  QuizScreen: {
    screen: QuizScreen,
  },
  UserStats: {
    screen: UserStats,
  },
  LoginScreen: {
    screen: LoginScreen,
  },
},
{
  initialRouteName: 'LoginScreen',
  /* The header config from HomeScreen is now here */
  defaultNavigationOptions: {
    header: null
  },
}
);

export default createAppContainer(AppNavigator);


